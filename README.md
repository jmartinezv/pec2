Proyecto realizado por Jaime Martínez Villar
-----------------------------------------------------
*						TUTORIAL DEL JUEGO					
-----------------------------------------------------
Este juego es un pequeño fragmento del clásico juego Super Mario Bros de Nintendo, recreado en Unity usando su motor de físicas.

El juego es un sencillo plataformas, usando las flechas de control podemos mover a Mario; siendo la flecha hacia arriba para saltar,
como nota, también se puede usar la barra espaciadora para dicha función.

Nuestro objetivo consistirá en llegar al final del nivel, recogiendo en nuestro camino el máximo número de monedas posibles, 
así como eliminando a los enemigos que nos encontremos, todo esto lo antes posible para obtener una mayor puntuación.

En la parte superior de la pantalla se muestra nuestra puntuación, a la izquierda, las monedas que llevamos recolectadas, y el 
tiempo restante que nos queda para superar el nivel.

Mario dispone de 3 vidas al comenzar la partida, estas pueden perderse de diferentes maneras: Siendo golpeado por un enemigo,
cayendo en un foso o que el tiempo disponible llegue a 0. 
Cuando Mario no disponga de más vidas tendrá su última oportunidad y en caso de fracasar, el juego concluirá.

Para obtener monedas, Mario debe golpear los bloques con un signo (?), los bloques de ladrillos no suelen contener nada, pero
alguno puede que tenga una buena cantidad de monedas.

Para eleminar a los enemigos Mario tiene dos opciones; saltar sobre ellos y aplastarlos o golpear un bloque por el que esté
pasando el enemigo. Ambas opciones recompensarána a Mario con puntos.

A parte de monedas, Mario puede encontrarse con unas Setas Especiales, si las recoge Mario pasará a modo "Super"; su tamaño
aumentará y su fuerza también, con lo que será capaz de romper los bloques de ladrillo. Además, si un enemigo lo golpease 
mientras está en modo Super Mario, no perdería una vida, sino que retornaria a su estado normal; en ese momento, se dispone
de un breve tiempo de invulnerabilidad para alejarse del peligro.  

Podemos pausar el juego en cualquier momento pulsando 'Esc'. En el menú de pausa podremos reiniciar la partida o cerrar el juego.

La partida acaba cuando Mario pierde todas sus vidas o cuando consigue llegar al final del nivel; donde posteriormente se nos
mostrará nuestra puntuación final y podremos elegir si empezar otra partida o salir.


---------------------------------------------------------
*						EXPLICACIÓN DEL CÓDIGO					
---------------------------------------------------------
El juego se divide en 3 escenas: Una escena Inicial, donde se muestran las vidas del jugador y se pide una confirmación para 
continuar; la escena de Juego, la recreación del nivel 1-1 del Super Mario; y la pantalla de Game Over, que servirá tanto
para cuando el jugador se queda sin vidas como para cuando este completa el nivel.

En la Pantalla "Inicio" se nos presenta el HUD: Puntuación en la esquina superior izquierda, monedas recogidas en el centro a la izquierda,
nombre del nivel (World 1-1) en el centro a la derecha y el contador de tiempo en la esquina superior derecha. También aparece un 
mensaje intermitente pidiendo pulsar cualquier botón para comenzar.

El cambio de escena se ejecuta a través del Script "StartLevel"; el cual se encarga tanto del parpadeo del mensaje como en cargar
la siguiente pantalla.

En el momento que se pulse cualquier tecla comenzará el juego; se hará de manera evidente en cuanto la característica música
comienza a sonar.

La Scene "World 1-1" es todo el nivel completo; se puede clasificar en distintos grupos:
	- Main Camera: Cámara del juego que sigue a Mario allá donde vaya. Esta gobernado por el Script "CameraController" y posee un AudioSource
	del cual escucharemos la melodía principal, así como otras melodías primarias como el aviso de que queda poco tiempo o la melodía cuando
	Mario pierde una vida.
	- Canvas: El HUD. Mantiene los contadores de puntuación y monedas y el tiempo restante. Aquí también encontrames el menú de Pausa, oculto
	hasta que se pulse el botón correspondiente.
	- BackGround: El fondo del juego; formado por 5 repeticiones de un conjunto de sprites. Se ha metido aquí también el GameLimit o los muros
	invisibles que impiden a Mario salirse de la zona de juego.
	- Ground: Formado tanto por el suelo físico, realizado con un TileMap, como por los obstaculos (tuberías, escalinatas y demás). 
	- Player: El jugador, Mario, controlado mediante el Script PlayerController.
	- Blocks: El conjunto de todos los bloques, tanto de ladrillo como de (?). Están organizados en las distintas "figuras" y filas que 
	conforman. Estos bloques están gobernados por el Script "QuestionBlock".
	- Enemies: Formado por el conjunto de enemigos. Estos han sido colocados manualmente (debido a su pequeña cantidad) pero no estarán
	activos hasta que no entren en el campo de visión del jugador. El Script "EnemyAI" controla a los enemigos.
	- GameController: El controlador del juego, lleva la cuenta de las puntuaciones y estado del juego. Como hijo tiene la meta a la que
	Mario debe llegar para controlar el nivel.
	- PauseScreenManager: Controla la pantalla de Pausa; se ha reutilizado y ajustado de la PEC anterior.
	
	
- DECISIONES DE DISEÑO EN LA PROGRAMACIÓN -

- CameraController -
Lo principal era conseguir que la cámara siguiese a Mario en su movimiento, para ello se actualiza el transform.position de la cámara
en el Update, a través de la función Mathf.SmoothDamp. Esta función hace que la posición de la cámara cambie de forma gradual, de manera
que no se mueva bruscamente. La delimitación de la posición de la cámara viene dada por los colliders del GameLimit; utilizando la 
estructura bounds.size.

Aquí también actualizamos la música de fondo a través del estado que nos trasmite el GameController:
	- Theme principal (smb_theme), el tema principal del juego.
	- Fanfarria Final (smb_stage_clear), el cuál sonará cuando consigamos llegar a la meta.
	- Aviso de TimeUp (smb_warning), melodía de aviso que suena cuando queda 1/4 del tiempo inicial.
	- Theme principal acelerado (smb_hurry_up), una versión del tema principal con un ritmo más acelerado para indicar que el 
	tiempo se agota.

1 - GameController -
Nada en especial que destacar, actualiza la visualización de los puntos, monedas y el tiempo restante; así como las vidas que le quedan
al jugador.

2 - PlayerController -
Se ha utilizado un enum (PlayerState) para indicar el estado del jugador para actualizar las variables y sprites de animación.

Mientras el juego no haya terminado (GameController.finish) y el jugador esté vivo (PlayerState.dead) actualiza las animaciones:

void UpdateStateAnimation()
Aunque por el nombre parece que sólo afecta a las animaciones también lo hace a las dimensiones del jugador en sí; dependiendo de su
estado de poder (playerPower) siendo 0 el estado normal y 1 el estado Super, tanto el colisionador (BoxCollider2D) como la posición de
los pies cambia. El colisionador también cambia si Mario está saltando o agachado (sólo en modo Super).

Tras ello analiza el estado del jugador y activa (o desactiva) los booleanos del Animator para transicionar entre los distintos sprites.
Destacar que en las animaciones de transformación (Normal -> Super y viceversa) llama a unas subrutinas para controlar diferentes aspectos;
ya sea sonido, animaciones intermedias o el tiempo de invenibilidad cuando Mario es golpeado por un enemigo en modmo Super.

Además, si se está llevando a cabo la transformación a Super Mario, se impide que el juego actualice el input del jugador (y el movimiento
del enemigo como veremos más adelante).
	
void UpdateInput()
Llama a las rutinas de movimiento según la tecla pulsada. Destacar que, mientras que el movimiento horizontal se hace a través de un 
GetButton, el salto se hace a través de GetButtonDown. Esto se hace para conseguir una diferenciación en el salto de cuando se pulsada
levemente a cuando lo dejamos pulsado, a través de un contador que va añadiendo impulso si no hemos levantado el dedo del botón de salto.

Otra cosa destacable es que el aslto no es homogéneo: La subida y la caída no son iguales. A la caída se añade un vector de gravedad al 
RigidBody del jugador para dar una mayor control y una sensación más cómoda.

void RightMovement()/LeftMovement()
Primero analiza si está tocando el suelo a través de la función GroundDetection() y entonces actúa en consecuencia.
Básicamente va añadiendo al vector velocidad del RigidBody en función de la variable runSpeed hasta un máximo, denominado "maxSpeedRunning",
por lo que de llegar a sobrepasar dicha velocidad, el vector velocidad del rb se igualaria a esta velocidad máxima.

Si además Mario estaba corriendo en dirección contraria y llevaba una velocidad de, al menos, la mitad de la máxima, se observará cómo realiza
un giro rápido. 
Otra cosa a tener en cuenta es que si está en modo Super y está agachado, aumentamos el coeficiente de rozamiento para que no pueda ir tan 
rápido como corriendo de forma normal.

void GroundDetection()
Realiza 3 Raycasts desde la posición de los pies, un GameObject vacío colocado en la posición deseada (feet.transform.position), de MUY corta
distancia para determinar si está sobre suelo. Los rayos salen desde cada extremo del colisionador como del centro de Mario, y si uno de ellos
NO es nulo, indica que está sobre una superficie. 
Destacar que dicha superficie debe hallarse en la Capa (Layer) creada denominada "Ground".

void CeilingCollision()
Al igual que GroundCollision pero hacia arriba. Nos permite hacer que Mario caiga si detecta que ha chocado con un obstaculo, y si este 
obstaculo es un bloque, llama a su función BlockBounce() que describiremos más adelante.
Para detectar los bloques utilizamos la Etiqueta (Tag) "QuestionBlock".

3 - EnemyAI -
Al contrario que Mario, los enemigos los he controlado modificando directamente su posición en el transform; es decir, sin utilizar físicas.
Esto se ha hecho así por dos motivos:
	1- Comprobar de manera efectiva la diferencia de un cuerpo con movimiento simple a uno con físicas.
	2- Los movimientos de los Goombas eran muy simples.
	3- Se parece más al juego original.
	
Algunas de las funciones características de este Script son:

void Death() y void Dying()
Ejecutadas cuando el enemigo muere, la primera controla la animación de la muerte y los puntos que se obtienen al eliminar al enemigo, mientras
que la segunda controla un temporizador tras el cuál se destruye el objeto (Destroy()).

void GroundCollision()
Igual que el del PlayerController pero con el añadido de que si está caminando sobre un bloque y el jugador golpea dicho bloque, se llama a la rutina
de muerte del enemigo (Death()).

void CheckWallCollision()
Otro Raycast como el de GroundCollision pero único y horizontal. Permite detectar si el Goomba ha chocado contra un muro u otro Goomba para poder
cambiar su dirección de movimiento.

void OnCollisionEnter2D(Collision2D collision)
Detecta si el colisionador toca al jugador, a través de la Tag "Player", y en caso afirmativo obtiene el punto de contacto y comprueba si está
por encima o por debajo de su "Punto débil" (WeakPoint, un GameObject vacío como los pies de Mario).
Si está sobre el Punto Débil, el goomba muere y Mario obtiene un pequeño impulso, si está por debajo Mario pierde una vida a menos que estuviese
en modo Super, en cuyo caso vuelve al estado normal.

4 - QuestionBlock -
A pesar del nombre está en todos los bloques, tanto de (?) como de ladrillos, y sus diferencias vendrán dadas por las variables editables en el
Inspector:
	spawnPrefab: Indica el objeto que se obtiene al golpear el bloque; puede estar vacío.
	spawnSpeed y spawnHeight: Se usa en principio sólo para las monedas e indican la velocidad y la altura de la animación al obtener el contendio.
	quantity: La cantidad de objetos que se pueden obtener del bloque; suele ser 1 o 0 pero hay un bloque con 10 monedas.
	isBreakable: Indica que el bloque se puede romper si Mario está en modo Super.
	
El movimiento (bote) del bloque está controlado por una subrutina, así como el movimiento de la moneda que se instancia.


5- PowerUP - 
Controla el movimiento del PowerUP así como detectar que Mario lo ha cogido. Para los PowerUPs se han vuelto a usar físicas, simplemente por el
hecho de diferenciar dos movimientos simples como es el del PowerUP y los Goombas. Sus funciones son simples, como destacable se puede decir que en
su "GroundDetection" si está sobre un bloque y es golpeado por el jugador, el PowerUP cambia de dirección.

También destacar que en el GameController se ha añadido está instrucción: 

        Physics2D.IgnoreLayerCollision(10, 11);
Siendo las capas 10 y 11 "Enemy" y "PowerUP" respectivamente, para evitar que los Goombas colisionen con los PowerUPs y cambien de dirección o se 
impidan moverse el uno al otro.









