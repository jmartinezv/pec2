﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {
    public Text score;
    public Text coinCount;
    public Text scoreBig;

    private void Start()
    {
        score.text = GameController.points.ToString("000000");
        coinCount.text = "x" + GameController.coins.ToString("00");
        scoreBig.text = "Your Score: " + GameController.points.ToString("000000");
    }

    void Update () {
        if (Input.GetKey(KeyCode.Escape))
            GameExit();
        if (Input.GetKey(KeyCode.Space))
            RestartGame();
	}

    /// <summary>
    /// Función para cerrar el juego.
    /// </summary>
    void GameExit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Función para reiniciar el juego con una partida nueva.
    /// </summary>
    void RestartGame()
    {
        GameController.lifes = 3;
        GameController.points = 0;
        GameController.coins = 0;
        GameController.finish = false;
        
        SceneManager.LoadScene("Inicio");
    }
}
