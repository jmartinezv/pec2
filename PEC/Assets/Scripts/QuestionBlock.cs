﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class QuestionBlock : MonoBehaviour {

    public float bounce;
    public float speed;
    public GameObject spawnPrefab;
    public float spawnSpeed;
    public float spawnHeight;
    public int quantity;
    public bool isBreakable;

    public bool bouncing;

    private Vector2 origionalPosition;
    private bool isReady = true;
    private int coinPoints = 200;
    private GameObject spawnObject;
    private AudioSource audioSource;
    AudioClip bump;
    AudioClip breakBlock;
    AudioClip spawnSound;
    private bool breaking;

    
    void Start () {
        origionalPosition = transform.localPosition;

        if (GetComponent<AudioSource>() != null)
            audioSource = GetComponent<AudioSource>();

        if (spawnPrefab != null)
        {
            if (spawnPrefab.tag == "Coin")
                spawnSound = Resources.Load<AudioClip>("Sounds/smb_coin");
            else if (spawnPrefab.tag == "PowerUP")
                spawnSound = Resources.Load<AudioClip>("Sounds/smb_powerup_appears");
        }

        bump = Resources.Load<AudioClip>("Sounds/smb_bump");
        breakBlock = Resources.Load<AudioClip>("Sounds/smb_breakblock");
        bouncing = false;
        breaking = false;
    }

    /// <summary>
    /// Función que hace que el cubo "bote" cuando el jugador lo 
    /// golpea desde abajo.
    /// </summary>
    public void BlockBounce()
    {
        if (isReady)
        {
            // Los bloques de ladrillos son rompibles cuando Mario está en forma Super.
            if(isBreakable && PlayerController.playerPower > 0)
            {
                audioSource.clip = breakBlock;
                GetComponent<Animator>().SetTrigger("isBroken");
                breaking = true;
            }
            else
                audioSource.clip = bump;

            audioSource.Play();

            StartCoroutine(Bounce());
        }
    }

    /// <summary>
    /// Instancia el objeto que aparece del cubo y ejecuta según dicho objeto.
    /// </summary>
    void CreateSpawneable()
    {
        quantity--;
        spawnObject = Instantiate(spawnPrefab);
        spawnObject.transform.parent = transform.parent;

        //Punto de spawn del objeto
        spawnObject.GetComponent<Transform>().localPosition = new Vector2(origionalPosition.x, origionalPosition.y + 0.5f);

        //En caso de ser una moneda aparece el texto de cuantos puntos se obtienen e incrementa el contador de monedas.
        if (spawnPrefab.tag == "Coin")
        {
            StartCoroutine(MoveCoin(spawnObject));
            spawnObject.GetComponentInChildren<Text>().text = coinPoints.ToString();
            GameController.coins++;
            GameController.points += coinPoints;
        }
        audioSource.clip = spawnSound;
        audioSource.Play();
    }

    /// <summary>
    /// Corrutina que mueve (simula el bote) del bloque.
    /// </summary>
    IEnumerator Bounce()
    {
        isReady = false;

        //Si tiene algo dentro lo instancia
        if (quantity >0)
            CreateSpawneable();

        //Si está vacío cambia su visualización.
        if (!isBreakable && quantity == 0)
            GetComponent<Animator>().SetTrigger("isEmpty");

        //Bote: Bloque Sube
        while (true)
        {
            transform.localPosition = new Vector2( transform.localPosition.x,  transform.localPosition.y + speed * Time.deltaTime);
            bouncing = true;

            if ( transform.localPosition.y >=  origionalPosition.y + bounce)
                break;

            yield return null;
        }

        //Bote: Bloque baja.
        while (true)
        {
             transform.localPosition = new Vector2( transform.localPosition.x,  transform.localPosition.y - speed * Time.deltaTime);

            if (transform.localPosition.y <= origionalPosition.y)
            {
                transform.localPosition = origionalPosition;
                bouncing = false;

                yield return new WaitForSeconds(0.25f);

                if (breaking)
                    Destroy(gameObject);
                else if (quantity > 0 || (isBreakable && PlayerController.playerPower == 0))
                    isReady = true;
                break;
            }

            yield return null;
        }
    }

    /// <summary>
    /// Corrutina que da movimiento a la moneda instanciada.
    /// </summary>
    IEnumerator MoveCoin(GameObject item)
    {
        while (true)
        {
            item.transform.localPosition = new Vector2(item.transform.localPosition.x, item.transform.localPosition.y + spawnSpeed * Time.deltaTime);

            if (item.transform.localPosition.y >= origionalPosition.y + spawnHeight)
                break;

            yield return null;
        }

        while (true)
        {
            item.transform.localPosition = new Vector2(item.transform.localPosition.x, item.transform.localPosition.y - spawnSpeed * Time.deltaTime);

            if (item.transform.localPosition.y <= origionalPosition.y + spawnHeight/2)
            {
                item.transform.localPosition = origionalPosition;
                Destroy(item.gameObject);
                break;
            }

            yield return null;
        }
    }


}
