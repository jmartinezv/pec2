﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public LayerMask groundedLayerMask;
    public Transform feet;

    public float maxJumpTime = 0.5f;
    public float runSpeed = 7f;
    public float jumpHeight = 4.5f;
    public float extraJump = 12f;
    public float maxSpeedRunning = 6f;
    public float groundedDistance = 0.05f;
    public float gravity = 6f;
    public float deathJump;
    public float crawlJump;

    public enum PlayerState
    {
        jumping,
        idle,
        moving,
        crawling,
        dead
    };

    private PlayerState playerState = PlayerState.idle;
    public static int playerPower;
    public static bool loadGame;
    public static bool invulnerabilty;

    private bool playerJumping;
    private float jumpTime;
    private Vector2 velocity;
    private bool quickStop;
    private Rigidbody2D rb;
    private SpriteRenderer mario;
    private BoxCollider2D playerCollider;
    private AudioSource audioSource;
    private Animator animator;
    private AudioClip jumpSound;
    private AudioClip deathSound;
    AudioClip jumpSuperSound;
    AudioClip powerPickUp;
    AudioClip powerDown;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        velocity = rb.velocity;
        mario = GetComponent<SpriteRenderer>();
        playerCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        jumpSound = Resources.Load<AudioClip>("Sounds/smb_jump-small");
        deathSound = Resources.Load<AudioClip>("Sounds/smb_death");
        jumpSuperSound = Resources.Load<AudioClip>("Sounds/smb_jump-super");
        powerPickUp = Resources.Load<AudioClip>("Sounds/smb_powerup");
        powerDown = Resources.Load<AudioClip>("Sounds/smb_pipe");
        playerPower = 0;
    }
	
	// Update is called once per frame
	void Update () {
        velocity = rb.velocity;
        if (!GameController.finish)
        {
            if (playerState != PlayerState.dead)
            {
                UpdateStateAnimation();

                if (!PowerUp.transformAnim)
                {
                    UpdateInput();
                }

            }
        }
        else
            animator.SetBool("isEnd", true);
    }

    /// <summary>
    /// Corrutina que indica y muestra que Mario se haya en un periodo de invulnerabilidad.
    /// </summary>
    IEnumerator InvulnerableFrames()
    {
        int count = 0;
        while (count < 15)
        {
            mario.enabled = false;
            yield return new WaitForSeconds(.1f);
            mario.enabled = true;
            yield return new WaitForSeconds(.1f);
            count++;
        }
        invulnerabilty = false;
    }

    /// <summary>
    /// Corrutina que convierte a Mario en SuperMario.
    /// </summary>
    IEnumerator Transformation()
    {
        animator.SetTrigger("growSuper");
        rb.simulated = false;
        rb.velocity = new Vector2(0,0);
        yield return new WaitForSeconds(powerPickUp.length);

        PowerUp.transformAnim = false;
        audioSource.Stop();
        rb.simulated = true;
    }

    /// <summary>
    /// Corrutina que espera a que suene la música de muerte para cargar partida.
    /// </summary>
    IEnumerator PlayDeathSound()
    {
        audioSource.Play();
        yield return new WaitForSeconds(deathSound.length);

        loadGame = true;
    }

    /// <summary>
    /// Actualiza la animación del sprite según el estado de Mario.
    /// </summary>
    void UpdateStateAnimation()
    {
        if (playerPower == 0)
            feet.localPosition = new Vector3(0, -0.25f, 0);
        else
            if (playerState == PlayerState.crawling)
                feet.localPosition = new Vector3(0, -0.34f, 0);
            else
                feet.localPosition = new Vector3(0, -0.5f, 0);

        if (!Input.anyKey || (Input.GetButton("Jump")))
        {
            if (GroundDetection())
            {
                if (rb.velocity.magnitude > 0.2f)
                    playerState = PlayerState.moving;
                else
                    playerState = PlayerState.idle;
            }
            else
                playerState = PlayerState.jumping;
        }

        //Estados del jugador en animación
        if (playerState == PlayerState.jumping)
        {
            animator.SetBool("isJumping", true);
            if (playerPower == 0)
                playerCollider.size = new Vector2(0.48f, 0.5f);
            else
                playerCollider.size = new Vector2(0.48f, 1f);
        }
        else
        {
            animator.SetBool("isJumping", false);
            if (playerPower == 0)
                playerCollider.size = new Vector2(0.38f, 0.5f);
            else
                playerCollider.size = new Vector2(0.48f, 1f);
        }

        if (playerState == PlayerState.moving)
            animator.SetBool("isMoving", true);
        else
            animator.SetBool("isMoving", false);


        if (playerState == PlayerState.crawling)
        {
            animator.SetBool("isCrawling", true);
            playerCollider.size = new Vector2(0.48f, 0.67f);
        }
        else
            animator.SetBool("isCrawling", false);

        if (PowerUp.transformAnim)
        {
            audioSource.clip = powerPickUp;
            if (!audioSource.isPlaying)
                audioSource.Play();

            StartCoroutine("Transformation");
        }

        if (EnemyAI.growSmall)
        {
            audioSource.clip = powerDown;
            if (!audioSource.isPlaying)
                audioSource.Play();

            invulnerabilty = true;
            animator.SetTrigger("growSmall");
            StartCoroutine("InvulnerableFrames");
            EnemyAI.growSmall = false;
        }

        if (GameController.playerDies)
        {
            animator.SetBool("isDead", true);
            rb.AddForce(new Vector2(0, deathJump));
            playerState = PlayerState.dead;
            playerCollider.enabled = false;
            audioSource.clip = deathSound;
            StartCoroutine("PlayDeathSound");
        }
    }

    /// <summary>
    /// Actualiza el movimiento de Mario según la tecla pulsada.
    /// </summary>
    private void UpdateInput()
    {
        //INPUT MOVIMIENTOS
        if (Input.GetKey(KeyCode.RightArrow))
            RightMovement();
        if (Input.GetKey(KeyCode.LeftArrow))
            LeftMovement();

        if (Input.GetButtonDown("Jump") && GroundDetection())
        {
            playerJumping = true;
            jumpTime = maxJumpTime;
            Jump();
        }

        if (Input.GetButtonUp("Jump") || jumpTime <= 0 || CeilingCollision())
        {
            playerJumping = false;
        }

        if (playerJumping)
        {
            jumpTime -= Time.deltaTime;
            playerState = PlayerState.jumping;

            if (jumpTime > 0)
                rb.AddForce(Vector2.up * extraJump);
        }

        if (Input.GetKey(KeyCode.DownArrow) && playerPower > 0)
            playerState = PlayerState.crawling;


        if (!GroundDetection() && !playerJumping)
            rb.velocity -= new Vector2(0, gravity) * Time.deltaTime;
    }

    /// <summary>
    /// Movimiento hacia la derecha
    /// </summary>
    private void RightMovement()
    {
        if (GroundDetection())
        {
            if (mario.flipX && rb.velocity.magnitude >= maxSpeedRunning/2)
            {
                animator.SetTrigger("quickStop");
                quickStop = true;
            }
            mario.flipX = false;

            if (velocity.x < maxSpeedRunning || quickStop)
            {
                rb.velocity += new Vector2(transform.right.x * runSpeed, transform.right.y * runSpeed) * Time.deltaTime;
                quickStop = false;
            }
            else
            {
                velocity.x = maxSpeedRunning;
                rb.velocity = new Vector2(velocity.x, velocity.y);
            }

            if (Input.GetKey(KeyCode.DownArrow) && playerPower > 0)
                rb.drag = 6f;
            else
            {
                rb.drag = 3f;
                playerState = PlayerState.moving;
            }
        }
        else
        {
            if (velocity.x < maxSpeedRunning)
                rb.velocity += new Vector2(transform.right.x * runSpeed, transform.right.y * runSpeed) * Time.deltaTime;
            else
            {
                velocity.x = maxSpeedRunning;
                rb.velocity = new Vector2(velocity.x, velocity.y);
            }
        }
       
    }

    /// <summary>
    /// Movimiento hacia la izquierda.
    /// </summary>
    private void LeftMovement()
    {
        if (GroundDetection())
        {
            if (!mario.flipX && rb.velocity.magnitude >= maxSpeedRunning/2)
            {
                animator.SetTrigger("quickStop");
                quickStop = true;
            }
            mario.flipX = true;

            if (velocity.x < maxSpeedRunning || quickStop)
            {
                rb.velocity -= new Vector2(transform.right.x * runSpeed, transform.right.y * runSpeed) * Time.deltaTime;
                quickStop = false;
            }
            else
            {
                velocity.x = -maxSpeedRunning;
                rb.velocity = new Vector2(velocity.x, velocity.y);
            }

            if (Input.GetKey(KeyCode.DownArrow) && playerPower > 0)
                rb.drag = 6f;
            else
            {
                rb.drag = 3f;
                playerState = PlayerState.moving;
            }
        }
        else
        {
            if (velocity.x > -maxSpeedRunning)
                rb.velocity -= new Vector2(transform.right.x * runSpeed, transform.right.y * runSpeed) * Time.deltaTime;
            else
            {
                velocity.x = -maxSpeedRunning;
                rb.velocity = new Vector2(velocity.x, velocity.y);
            }
        }


    }

    /// <summary>
    /// Salto.
    /// </summary>
    private void Jump()
    {
        if (playerState == PlayerState.crawling)
            rb.AddForce(Vector2.up * crawlJump, ForceMode2D.Impulse);
        else
            rb.AddForce(Vector2.up * jumpHeight, ForceMode2D.Impulse);

        if (playerPower == 0)
            audioSource.clip = jumpSound;
        else
            audioSource.clip = jumpSuperSound;

        if (!audioSource.isPlaying)
            audioSource.Play();
    }

    /// <summary>
    /// Realiza 3 raycast hacia abajo para detectar una superficie por la que caminar.
    /// </summary>
    bool GroundDetection()
    {
        RaycastHit2D rightHit;
        RaycastHit2D leftHit;
        RaycastHit2D midHit;
        float rate = playerCollider.size.x / 2;
        Debug.DrawRay(new Vector2(feet.transform.position.x + rate, feet.transform.position.y), Vector2.down, Color.green);
        Debug.DrawRay(new Vector2(feet.transform.position.x - rate, feet.transform.position.y), Vector2.down, Color.green);
        Debug.DrawRay(feet.transform.position, Vector2.down, Color.green);

        rightHit = Physics2D.Raycast(new Vector2(feet.transform.position.x + rate, feet.transform.position.y), Vector2.down, groundedDistance, groundedLayerMask);
        leftHit = Physics2D.Raycast(new Vector2(feet.transform.position.x - rate, feet.transform.position.y), Vector2.down, groundedDistance, groundedLayerMask);
        midHit = Physics2D.Raycast(feet.transform.position, Vector2.down, groundedDistance, groundedLayerMask);

        if (midHit.collider != null || leftHit.collider != null || rightHit.collider != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// Realiza 3 raycast hacia arriba para detectar si colisiona con un bloque; en cuyo caso 
    /// llama a la función de Btoe (BlockBounce) de dicho bloque.
    /// </summary>
    bool CeilingCollision()
    {
        RaycastHit2D rightHit;
        RaycastHit2D leftHit;
        RaycastHit2D midHit;
        Vector2 head = new Vector2(transform.position.x, transform.position.y);
        float width = playerCollider.size.x / 2;
        float height = playerCollider.size.y / 2;

        Debug.DrawRay(new Vector2(head.x + width, head.y + height), Vector2.up, Color.red);
        Debug.DrawRay(new Vector2(head.x - width, head.y + height), Vector2.up, Color.red);
        Debug.DrawRay(new Vector2(head.x, head.y + height), Vector2.up, Color.red);

        rightHit = Physics2D.Raycast(new Vector2(head.x + width, head.y + height), Vector2.up, groundedDistance, groundedLayerMask);
        leftHit = Physics2D.Raycast(new Vector2(head.x - width, head.y + height), Vector2.up, groundedDistance, groundedLayerMask);
        midHit = Physics2D.Raycast(new Vector2(head.x, head.y + height), Vector2.up, groundedDistance, groundedLayerMask);


        if (midHit.collider != null || leftHit.collider != null || rightHit.collider != null)
        {
            RaycastHit2D hitRay = rightHit;

            if (rightHit)
                hitRay = rightHit;
            else if (midHit)
                hitRay = midHit;
            else if (leftHit)
                hitRay = leftHit;

            if (hitRay.collider.tag == "QuestionBlock")
                hitRay.collider.GetComponent<QuestionBlock>().BlockBounce();

            return true;
        }
        else
            return false;
    }
}
