﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartLevel : MonoBehaviour {

    public Text startText;
    public Text lifeText;

	// Use this for initialization
	void Start () {
        lifeText.text = "x" + GameController.lifes.ToString();
        StartCoroutine("Flash");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKey)
            SceneManager.LoadScene("World 1-1");
	}
    

    IEnumerator Flash()
    {
        while (true)
        {
            startText.enabled = false;
            yield return new WaitForSeconds(.5f);
            startText.enabled = true;
            yield return new WaitForSeconds(.5f);
        }
    }
}
