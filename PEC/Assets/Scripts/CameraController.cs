﻿using System.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform player;
    public Transform leftBounds;
    public Transform rightBounds;
    public float movementTransition = 0.2f;

    /// <summary>
    /// Indica al GameController que espere antes de cargar la Pantalla Final cuando se completa el nivel
    /// </summary>
    public static bool waitForEnd;

    Vector3 offset;
    private float camWidth, camHeight, camMinX, camMaxX;
    private Vector3 movementVelocity = Vector3.zero;
    AudioClip themeSound;
    AudioClip endSound;
    AudioClip timeSound;
    AudioClip hurryUp;
    AudioSource audioSource;
    private bool isWarning;

   
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        themeSound = Resources.Load<AudioClip>("Sounds/smb_theme");
        endSound = Resources.Load<AudioClip>("Sounds/smb_stage_clear");
        timeSound = Resources.Load<AudioClip>("Sounds/smb_warning");
        hurryUp = Resources.Load<AudioClip>("Sounds/smb_hurry_up");
        isWarning = false;
        //Carga la música principal
        audioSource.clip = themeSound;
        audioSource.Play();

        waitForEnd = false;

        //Establece el aspecto de la cámara
        camHeight = Camera.main.orthographicSize * 2;     //orthographicSize es la mitad de la altura de la camera cuando esta está en modo ortografico
        camWidth = camHeight * Camera.main.aspect;

        float leftBoundsWidth = leftBounds.GetComponentInChildren<BoxCollider2D>().bounds.size.x / 2;
        float rightBoundsWidth = leftBounds.GetComponentInChildren<BoxCollider2D>().bounds.size.x / 2;

        //Establece la posicioón máxima y minima de la cámara (para que no se observe el límite invisible del juego)
        camMinX = leftBounds.position.x + leftBoundsWidth + (camWidth / 2);
        camMaxX = rightBounds.position.x - rightBoundsWidth - (camWidth / 2);
    }


    void Update()
    {
        //Compara la posicion para obtener el mayor valor entre, el nivel minimo, y el nivel maximo O la posicion del jugador para que la camara no se vaya más allá
        float playerX = Mathf.Max(camMinX, Mathf.Min(camMaxX, player.position.x));
        float x = Mathf.SmoothDamp(transform.position.x, playerX, ref movementVelocity.x, movementTransition);

        transform.position = new Vector3(x, transform.position.y, transform.position.z);

        UpdateMusic();
    }

    /// <summary>
    /// Actualiza la música de fondo según las condiciones del juego.
    /// </summary>
    void UpdateMusic()
    {
        if (GameController.finish && audioSource.clip != endSound)
        {
            audioSource.clip = endSound;
            audioSource.Play();
            waitForEnd = true;
        }
        else if (GameController.warning && !isWarning)
        {
            isWarning = true;
            audioSource.clip = timeSound;
            StartCoroutine("WarningToHurry");
        }
        
        if (GameController.playerDies)
            audioSource.Stop();

        if (PauseScreenManager.gamePaused)
            audioSource.Pause();
        else
            audioSource.UnPause();

    }

    /// <summary>
    /// Espera a que suene la música de falta de tiempo antes de que empiece a sonar la principal
    /// acelerada.
    /// </summary>
    IEnumerator WarningToHurry()
    {
        audioSource.Play();
        yield return new WaitForSeconds(timeSound.length);
        audioSource.clip = hurryUp;
        audioSource.Play();
    }
}   
