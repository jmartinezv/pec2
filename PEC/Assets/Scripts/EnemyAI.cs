﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAI : MonoBehaviour
{
    public LayerMask maskLayers;
    public LayerMask groundedLayerMask;
    public Transform weakSpot;
    public bool isWalkingLeft = true;
    public float walkSpeed;
    public float collisionDistance;
    public float disapearTime;
    public float deathJump;
    public float gravity;
    public int points;
    public float deathHop;

    //Indica transición de SUper->Normal.
    public static bool growSmall;

    private Text deathPoints;
    private bool isDead;
    private float deathTimer;
    Vector3 pos;
    BoxCollider2D enemyCollider;
    AudioSource audioSource;
    AudioClip stompSound;
    private bool stomped;


    void Start()
    {
        deathPoints = GetComponentInChildren<Text>(); 
        enabled = false;
        enemyCollider = GetComponent<BoxCollider2D>();
        audioSource = GetComponent<AudioSource>();
        stompSound = Resources.Load<AudioClip>("Sounds/smb_stomp");
        
    }


    void Update()
    {
        
        if (!isDead)
        {
            //Previene el movimiento de los Goombas cuando se recoge un PowerUP.
            if (!PowerUp.transformAnim)
                Movement();
        }
        else
            Dying();
    }

    /// <summary>
    /// Actualiza la posición de los Goombas; ya sea horizontal o en caída.
    /// </summary>
    void Movement()
    {
        pos = transform.localPosition;
        if (GroundDetection())
        {
            if (isWalkingLeft)
                pos.x -= walkSpeed * Time.deltaTime;
            else
                pos.x += walkSpeed * Time.deltaTime;
            
            CheckWallCollision();
        }
        else
            pos.y -= gravity * Time.deltaTime;

        transform.position = pos;

    }

    /// <summary>
    /// Espera un tiempo antes de hacer desaparecer al enemigo muerto.
    /// </summary>
    void Dying()
    {
        if (deathTimer <= disapearTime)
            deathTimer += Time.deltaTime;
        else
            Destroy(gameObject);
    }

    /// <summary>
    /// Función que se ejecuta cuando el enemigo muere; se transiciona a la animación correspondiente y 
    /// se elimina su colisionador para que no interfiera mientras desaparece. Añade los puntos al contador.
    /// </summary>
    void Death()
    {
        if (stomped)
        {
            GetComponent<Animator>().SetBool("isDead", true);
            GetComponent<Rigidbody2D>().simulated = false;
            isDead = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipY = true;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, deathHop));
        }
        StartCoroutine("DeathText");
        enemyCollider.enabled = false;
        GameController.points += points;
        audioSource.clip = stompSound;
        audioSource.Play();
    }

    /// <summary>
    /// Muestra los puntos obtenidos por matar al enemigo por en breve periodo de tiempo.
    /// </summary>
    IEnumerator DeathText()
    {
        deathPoints.text = points.ToString();
        yield return new WaitForSeconds(.5f);
        deathPoints.enabled = false;
    }

    // Analiza si colisiona con el jugador; en caso afirmativo, analiza el punto de contacto en referencia a su
    // "punto débil" (Trasform).
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            float height = collision.GetContact(0).point.y - weakSpot.position.y;

            if (height > 0.1f)
            {
                collision.rigidbody.AddForce(new Vector2(0, deathJump));
                stomped = true;
                Death();
            }
            else
            {
                if (!PlayerController.invulnerabilty)
                {
                    if (PlayerController.playerPower == 0)
                    {
                        GameController.playerDies = true;
                    }
                    else
                    {
                        PlayerController.playerPower--;
                        growSmall = true;
                    }

                }
            }

        }
    }

    /// <summary>
    /// Realiza un Raycast horizontal para detectar colisión con el entorno y cambiar de dirección.
    /// </summary>
    void CheckWallCollision()
    {
        RaycastHit2D midHit;
        Vector2 direction;
        Vector2 pos;

        if (isWalkingLeft)
        {
            direction = Vector2.left;
            pos = new Vector2(transform.position.x - enemyCollider.size.x / 2 - 0.01f, transform.position.y);
        }
        else
        {
            direction = Vector2.right;
            pos = new Vector2(transform.position.x + enemyCollider.size.x / 2 + 0.01f, transform.position.y);
        }

        Debug.DrawRay(pos, direction, Color.green);

        midHit = Physics2D.Raycast(pos, direction, collisionDistance, maskLayers);

        if (midHit.collider != null && midHit.collider.gameObject != this.gameObject)
            isWalkingLeft = !isWalkingLeft;

    }

    /// <summary>
    /// Realiza 3 raycast hacia abajo para detectar una superficie por la que caminar.
    /// En caso de que esa superficie sea un bloque, analiza si ha sido golpeado por el jugador,
    /// pues en caso afirmativo el enemigo muere.
    /// </summary>
    bool GroundDetection()
    {
        RaycastHit2D rightHit;
        RaycastHit2D leftHit;
        RaycastHit2D midHit;
        float rate = enemyCollider.size.x / 2;
        Debug.DrawRay(new Vector2(transform.position.x + rate, transform.position.y - 0.25f), Vector2.down, Color.green);
        Debug.DrawRay(new Vector2(transform.position.x - rate, transform.position.y - 0.25f), Vector2.down, Color.green);
        Debug.DrawRay(new Vector2(transform.position.x, transform.position.y - 0.25f), Vector2.down, Color.green);

        rightHit = Physics2D.Raycast(new Vector2(transform.position.x + rate, transform.position.y - 0.25f), Vector2.down, 0.05f, groundedLayerMask);
        leftHit = Physics2D.Raycast(new Vector2(transform.position.x - rate, transform.position.y - 0.25f), Vector2.down, 0.05f, groundedLayerMask);
        midHit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.25f), Vector2.down, 0.05f, groundedLayerMask);
        
        if (midHit.collider != null || leftHit.collider != null || rightHit.collider != null)
        {
            if (leftHit.collider)
            {
                if (leftHit.collider.gameObject.tag == "QuestionBlock")
                    if (leftHit.collider.gameObject.GetComponent<QuestionBlock>().bouncing)
                        Death();
            }

            if (midHit.collider)
            {
                if (midHit.collider.gameObject.tag == "QuestionBlock")
                    if (midHit.collider.gameObject.GetComponent<QuestionBlock>().bouncing)
                        Death();
            }

            if (rightHit.collider)
            {
                if (rightHit.collider.gameObject.tag == "QuestionBlock")
                    if (rightHit.collider.gameObject.GetComponent<QuestionBlock>().bouncing)
                        Death();
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    //Previene que los goombas empiecen a actuar antes de que el jugador los pueda ver. 
    private void OnBecameVisible()
    {
        enabled = true;
    }
}
