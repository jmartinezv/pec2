﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreenManager : MonoBehaviour {

    public GameObject pauseScreen;

    public static bool gamePaused;

    private bool hasPlayed;

    public void QuitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Función para reiniciar el juego con una partida nueva.
    /// </summary>
    public void RestartScene()
    {
        GameController.lifes = 3;
        GameController.points = 0;
        GameController.coins = 0;
        GameController.finish = false;

        SceneManager.LoadScene("Inicio");
    }

    public void ContinueGame()
    {
        pauseScreen.SetActive(!pauseScreen.activeSelf);
    }

    private void Update()
    {
        if (pauseScreen.activeSelf)
        {
            if (!hasPlayed) {
                GetComponent<AudioSource>().Play();
                hasPlayed = true;
            }

            gamePaused = true;
            Time.timeScale = 0;
        }
        else
        {
            hasPlayed = false;
            Time.timeScale = 1;
            gamePaused = false;
        }

        if (Input.GetKeyUp(KeyCode.Escape))
            pauseScreen.SetActive(!pauseScreen.activeSelf);
    }
}
