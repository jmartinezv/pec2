﻿using System.Collections;
using UnityEngine;

public class PowerUp : MonoBehaviour {
    public int powerUpPoints = 1000;
    public LayerMask groundedLayerMask;
    public float velocidadLineal = 1f;

    public static bool transformAnim;

    private BoxCollider2D col;
    private Rigidbody2D rb;
    private bool isWalkingLeft;
    private bool spawned;
    Vector3 pos;

	// Use this for initialization
	void Start () {
        col = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        isWalkingLeft = false;
        StartCoroutine("WaitForSpawn");
    }

    /// <summary>
    /// Corrutina que evita que el objeto se mueva hasta que no haya sido instanciado del todo.
    /// </summary>
    IEnumerator WaitForSpawn()
    {
        spawned = false;
        yield return new WaitForSeconds(.3f);
        spawned = true;
    }

	// Update is called once per frame
	void Update ()
    {
        if (spawned == true)
        {
            //pos = transform.localPosition;

            if (GroundDetection())
            {
                if (isWalkingLeft)
                    rb.velocity = Vector2.left * velocidadLineal;
                else
                    rb.velocity = Vector2.right * velocidadLineal;                

                CheckWallCollision();
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (PlayerController.playerPower < 1)
                PlayerController.playerPower++;

            transformAnim = true;

            GameController.points += powerUpPoints;
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Realiza 3 raycast hacia abajo para detectar una superficie por la que caminar.
    /// </summary>
    bool GroundDetection()
    {
        RaycastHit2D rightHit;
        RaycastHit2D leftHit;
        RaycastHit2D midHit;
        float rate = col.size.x / 2;
        Debug.DrawRay(new Vector2(transform.position.x + rate, transform.position.y - 0.25f), Vector2.down, Color.green);
        Debug.DrawRay(new Vector2(transform.position.x - rate, transform.position.y - 0.25f), Vector2.down, Color.green);
        Debug.DrawRay(new Vector2(transform.position.x, transform.position.y - 0.25f), Vector2.down, Color.green);

        rightHit = Physics2D.Raycast(new Vector2(transform.position.x + rate, transform.position.y - 0.25f), Vector2.down, 0.05f, groundedLayerMask);
        leftHit = Physics2D.Raycast(new Vector2(transform.position.x - rate, transform.position.y - 0.25f), Vector2.down, 0.05f, groundedLayerMask);
        midHit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.25f), Vector2.down, 0.05f, groundedLayerMask);
        
        if (midHit.collider != null || leftHit.collider != null || rightHit.collider != null)
        {
            if (leftHit.collider)
            {
                if (leftHit.collider.gameObject.tag == "QuestionBlock")
                    if (leftHit.collider.gameObject.GetComponent<QuestionBlock>().bouncing)
                        ChangeDirection();
            }

            if (midHit.collider)
            {
                if (midHit.collider.gameObject.tag == "QuestionBlock")
                    if (midHit.collider.gameObject.GetComponent<QuestionBlock>().bouncing)
                        ChangeDirection();
            }

            if (rightHit.collider)
            {
                if (rightHit.collider.gameObject.tag == "QuestionBlock")
                    if (rightHit.collider.gameObject.GetComponent<QuestionBlock>().bouncing)
                        ChangeDirection();
            }
            
            return true;
        }
        else
            return false;
    }

    void ChangeDirection()
    {
        if (spawned)
            isWalkingLeft = !isWalkingLeft;
        StartCoroutine("WaitForSpawn");
    }

    /// <summary>
    /// Realiza un Raycast horizontal para detectar colisión con el entorno y cambiar de dirección.
    /// </summary>
    void CheckWallCollision()
    {
        RaycastHit2D midHit;
        Vector2 direction;
        Vector2 pos;

        if (isWalkingLeft)
        {
            direction = Vector2.left;
            pos = new Vector2(transform.position.x - col.size.x / 2 - 0.01f, transform.position.y);
        }
        else
        {
            direction = Vector2.right;
            pos = new Vector2(transform.position.x + col.size.x / 2 + 0.01f, transform.position.y);
        }

        Debug.DrawRay(pos, direction, Color.green);

        midHit = Physics2D.Raycast(pos, direction, 0.05f, groundedLayerMask);

        if (midHit.collider != null && midHit.collider.gameObject != this.gameObject)
            ChangeDirection();

    }
}
