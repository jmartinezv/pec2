﻿using System;
using UnityEngine;

public class FinishGame : MonoBehaviour {

    AudioSource audioSource;

    public static int finishPoints;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            audioSource.Play();
            int height = Convert.ToInt16(collision.GetContact(0).point.y);
            GameController.finish = true;

            if (height > 0)
                finishPoints = height * 1000;
            else
                finishPoints  = 500;
        }
    }
}
