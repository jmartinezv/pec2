﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public Text timeRemaining;
    public Text score;
    public Text coinCount;
    public Transform flag;
    //Velocidad de caída de la bandera cuando Mario llega al final.
    public float fallingSpeed;
    public int iniTime;

    /// <summary>
    /// Indica que el jugador pierde una vida.
    /// </summary>
    public static bool playerDies;
    /// <summary>
    /// Indica que el jugador ha llegado a la meta.
    /// </summary>
    public static bool finish;

    public static int coins;
    public static int points;
    public static int lifes = 3;
    /// <summary>
    /// Indica que queda poco tiempo.
    /// </summary>
    public static bool warning;
        
    private int time;
    private bool addedPoints = false;
    private float timer = 0.0f;
    private bool lifeLosed;
    Vector3 flagPos;
    AudioClip endSound;

 
    void Start () {
        playerDies = false;
        flagPos = flag.position;
        endSound = Resources.Load<AudioClip>("Sounds/smb_stage_clear");
        lifeLosed = false;
        //Previene que los Goombas (enemigos) y los PowerUPs (Setas) colisionen entre si.
        Physics2D.IgnoreLayerCollision(10, 11);

        PlayerController.loadGame = false;
        warning = false;
    }

    //Fosos 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            playerDies = true;
        else
            Destroy(collision.gameObject);
    }

    
    void Update () {
        UpdateTime();
        UpdateCoin();
        UpdateScore();
        UpdateLifes();

        // Hace caer la bandera final y muestra los puntos obtenidos por el salto.
        if (finish && flagPos.y >= 0.25f)
        {
            flagPos.y -= fallingSpeed * Time.deltaTime;
            flag.position = flagPos;
            flag.GetComponentInChildren<Text>().text = FinishGame.finishPoints.ToString();
        }

   	}

    /// <summary>
    /// Actualiza el tiempo restante, indicando estados según las condiciones.
    /// </summary>
    void UpdateTime()
    {
        if (!finish)
        {
            timer += Time.deltaTime;
            int seconds = Convert.ToInt32(timer);
            time = iniTime - seconds;

            if (time <= 100 && time > 0)
                warning = true;
            else if (time <= 0)
            {
                time = 0;
                playerDies = true;
            }
        }
        else
        {
            if (time > 0)
            {
                time--;
                points += 10;
            }
            if (CameraController.waitForEnd)
                StartCoroutine("WaitForEnd");
        }

        timeRemaining.text = time.ToString("000");
    }

    /// <summary>
    /// Actualiza el contador (visual) de monedas.
    /// </summary>
    void UpdateCoin()
    {
        coinCount.text = "x" + coins.ToString("00");
    }

    /// <summary>
    /// Actualiza el contador de puntos.
    /// </summary>
    void UpdateScore()
    { 
        if (finish && !addedPoints)
        {
            points += FinishGame.finishPoints;
            addedPoints = true;
        }
        score.text = points.ToString("000000");
    }

    /// <summary>
    /// Actualiza las vidas restantes del jugador y carga la partida según estas.
    /// </summary>
    void UpdateLifes()
    {
        if (playerDies)
        {
            if (!lifeLosed)
                lifes--;

            lifeLosed = true;
            if (PlayerController.loadGame == true)
            {
                if (lifes >= 0)
                    SceneManager.LoadScene("Inicio");
                else
                    SceneManager.LoadScene("GAME OVER");
            }
        }
    }

    /// <summary>
    /// Corrutina que espera que suene la fanfarria del final para mostrar la Pantalla de GAME OVER.
    /// </summary>
    IEnumerator WaitForEnd()
    {
        yield return new WaitForSeconds(endSound.length);
        SceneManager.LoadScene("GAME OVER");
    }
}
